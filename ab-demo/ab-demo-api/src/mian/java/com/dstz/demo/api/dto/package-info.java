/**
 * Project Name:demo
 * File Name:package-info.java
 * Package Name:com.dstz.demo.api
 * Date:2022年2月16日
 * Copyright (c) 2022, agilebpm.cn All Rights Reserved.
 *
 */
/**
 * ClassName: package-info <br/>
 * Function:  DTO 传输类型的模型，也可以是demo模块接口、实体的定义 <br/>
 * date: 2022年2月16日 <br/>
 *
 * @author jeff
 * @version 
 * @since JDK 17
 */
package com.dstz.demo.api.dto;