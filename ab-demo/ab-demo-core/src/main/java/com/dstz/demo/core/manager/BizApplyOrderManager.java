package com.dstz.demo.core.manager;

import com.dstz.demo.core.entity.BizApplyOrder;
import com.dstz.base.manager.AbBaseManager;

/**
 * <p>
 * 订单信息 通用业务类
 * </p>
 *
 * @author wacxhs
 * @since 2022-01-20
 */
public interface BizApplyOrderManager extends AbBaseManager<BizApplyOrder> {
}
