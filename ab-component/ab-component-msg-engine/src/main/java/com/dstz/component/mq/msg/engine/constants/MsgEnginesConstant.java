package com.dstz.component.mq.msg.engine.constants;

/**
 *
 * 消息实现常量
 * @author lightning
 */
public class MsgEnginesConstant {

    /**
     * 外部消息email接收人
     */
    public static final String EMAIL_RECEIVERS = "emailReceivers";

    /**
     * 外部消息sms接收人
     */
    public static final String SMS_RECEIVERS = "smsReceivers";

    public static final String  EMAIL_FILE_IDS ="emailFileIds";

    /**
     * 验证码
     */
    public static final String CODE = "code";
}
