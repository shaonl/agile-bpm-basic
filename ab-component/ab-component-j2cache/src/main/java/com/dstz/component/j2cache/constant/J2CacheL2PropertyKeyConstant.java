package com.dstz.component.j2cache.constant;

/**
 * j2cache 二级缓存属性键常量
 *
 * @author wacxhs
 */
public class J2CacheL2PropertyKeyConstant {

    /**
     * 是否开启
     */
    public static final String CACHE_OPEN = "cacheOpen";

    /**
     * 命名空间
     */
    public static final String NAMESPACE = "namespace";
}
