package com.dstz.springboot.autoconfigure.idgen;

/**
 * ID 生成器类型
 *
 * @author wacxhs
 */
public enum IdGeneratorType {

    /**
     * 雪花算法
     */
    SNOWFLAKE

}
