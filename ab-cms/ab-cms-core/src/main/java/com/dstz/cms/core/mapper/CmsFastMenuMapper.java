package com.dstz.cms.core.mapper;

import com.dstz.cms.core.entity.CmsFastMenu;
import com.dstz.base.mapper.AbBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 快捷菜单管理 Mapper 接口
 * </p>
 *
 * @author niu
 * @since 2022-03-11
 */
@Mapper
public interface CmsFastMenuMapper extends AbBaseMapper<CmsFastMenu> {

}
