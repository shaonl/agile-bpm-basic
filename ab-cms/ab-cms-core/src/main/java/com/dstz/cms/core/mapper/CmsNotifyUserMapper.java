package com.dstz.cms.core.mapper;

import com.dstz.cms.core.entity.CmsNotifyUser;
import com.dstz.base.mapper.AbBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 公告用户关联表 Mapper 接口
 * </p>
 *
 * @author niu
 * @since 2022-02-28
 */
@Mapper
public interface CmsNotifyUserMapper extends AbBaseMapper<CmsNotifyUser> {

}
