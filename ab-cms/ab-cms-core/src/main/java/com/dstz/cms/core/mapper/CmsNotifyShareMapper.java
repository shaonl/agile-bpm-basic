package com.dstz.cms.core.mapper;

import com.dstz.base.mapper.AbBaseMapper;
import com.dstz.cms.core.entity.CmsNotifyShare;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 公告发布对应部门表 Mapper 接口
 * </p>
 *
 * @author niu
 * @since 2022-03-01
 */
@Mapper
public interface CmsNotifyShareMapper  extends AbBaseMapper<CmsNotifyShare> {

}
