package com.dstz.base.model;

/**
 * datasource initialize schema
 *
 * @author wacxhs
 */
public interface AbDataSourceModeInitializeSchema {

	/**
	 * 初始化
	 *
	 * @param dataSourceModel datasource model
	 */
	void initialize(AbDataSourceModel dataSourceModel);

}
