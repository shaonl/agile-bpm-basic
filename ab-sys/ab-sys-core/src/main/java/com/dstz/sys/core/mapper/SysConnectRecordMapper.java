package com.dstz.sys.core.mapper;

import com.dstz.base.mapper.AbBaseMapper;
import com.dstz.sys.core.entity.SysConnectRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 公共业务关联记录 Mapper 接口
 * </p>
 *
 * @author jinxia.hou
 * @since 2022-02-17
 */
@Mapper
public interface SysConnectRecordMapper extends AbBaseMapper<SysConnectRecord> {

}
