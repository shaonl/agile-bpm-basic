package com.dstz.sys.core.mapper;

import com.dstz.base.mapper.AbBaseMapper;
import com.dstz.sys.core.entity.SysScript;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 常用脚本 Mapper 接口
 * </p>
 *
 * @author wacxhs
 * @since 2022-01-25
 */
@Mapper
public interface SysScriptMapper extends AbBaseMapper<SysScript> {

}
