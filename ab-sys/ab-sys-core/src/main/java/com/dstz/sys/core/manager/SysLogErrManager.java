package com.dstz.sys.core.manager;

import com.dstz.base.manager.AbBaseManager;
import com.dstz.sys.core.entity.SysLogErr;

/**
 * <p>
 * 系统异常日志 通用业务类
 * </p>
 *
 * @author jinxia.hou
 * @since 2022-02-17
 */
public interface SysLogErrManager extends AbBaseManager<SysLogErr> {

}
