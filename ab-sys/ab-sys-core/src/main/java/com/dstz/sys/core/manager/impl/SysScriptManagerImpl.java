package com.dstz.sys.core.manager.impl;

import com.dstz.base.manager.impl.AbBaseManagerImpl;
import com.dstz.sys.core.entity.SysScript;
import com.dstz.sys.core.manager.SysScriptManager;
import org.springframework.stereotype.Service;

/**
 * 常用脚本 通用服务实现类
 *
 * @author wacxhs
 * @since 2022-01-25
 */
@Service("sysScriptManager")
public class SysScriptManagerImpl extends AbBaseManagerImpl<SysScript> implements SysScriptManager {

}
