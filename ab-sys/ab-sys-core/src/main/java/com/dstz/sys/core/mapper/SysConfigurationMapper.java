package com.dstz.sys.core.mapper;

import com.dstz.sys.core.entity.SysConfiguration;
import com.dstz.base.mapper.AbBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lightning
 * @since 2023-05-11
 */
@Mapper
public interface SysConfigurationMapper extends AbBaseMapper<SysConfiguration> {

}
