package com.dstz.sys.rest.model.vo;

/**
 * <p>
 *
 * </p>
 *
 * @author lightning
 * @since 2023-05-11
 */
public class SysConfigurationVO  {
    private String id;
    private String code;
    private String name;
    private String desc;
    private Boolean enable;

    public SysConfigurationVO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
