package com.dstz.sys.core.manager;

import com.dstz.base.manager.AbBaseManager;
import com.dstz.sys.core.entity.SysScript;

/**
 * <p>
 * 常用脚本 通用业务类
 * </p>
 *
 * @author wacxhs
 * @since 2022-01-25
 */
public interface SysScriptManager extends AbBaseManager<SysScript> {

}
